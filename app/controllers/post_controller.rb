class PostController < ApplicationController
  before_action :authenticate_user!

  def index
    render json: {
        message: 'ok', status: 200
    }
  end
end